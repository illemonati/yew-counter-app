#!/bin/bash

OUTDIR="build"
STATICDIR="static"

mkdir -p $OUTDIR
cp -r $STATICDIR/* $OUTDIR
wasm-pack build --target web --out-name wasm --out-dir $OUTDIR

