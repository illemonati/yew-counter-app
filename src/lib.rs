use wasm_bindgen::prelude::*;
use yew::prelude::*;

struct Model {
    link: ComponentLink<Self>,
    value: isize
}

enum Msg {
    AddOne,
    SubOne,
    OpenPopUp,
}

impl Model {
    fn alert_value(&self) {
        let window = web_sys::window().expect("No Window Found");
        window.alert_with_message(&format!("{}", self.value)).expect("alert error");
    }
}

impl Component for Model {
    type Message = Msg;
    type Properties = ();
    fn create(_props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {
            link,
            value: 0,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::AddOne => self.value += 1,
            Msg::SubOne => self.value -= 1,
            Msg::OpenPopUp => self.alert_value(),
        }
        true
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        false
    }

    fn view(&self) -> Html {
        html! {
            <div>
                <p> {self.value} </p>
                <br />
                <button onclick=self.link.callback(|_| Msg::AddOne)> { "+" } </button>
                <button onclick=self.link.callback(|_| Msg::SubOne)> { "-" } </button>
                <br />
                <button onclick=self.link.callback(|_| Msg::OpenPopUp)> { "Open Popup Containing Number"} </button>
            </div>
        }
    }
}

#[wasm_bindgen(start)]
pub fn run_app() {
    App::<Model>::new().mount_to_body();
}


